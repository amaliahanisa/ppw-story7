from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone
from django.http import HttpRequest
import unittest
from .views import *


# Create your tests here.
class UnitTest(TestCase):
    def test_url_valid(self):
        response = Client().get('/#')
        self.assertEqual(response.status_code, 200)

    def test_index_html(self):
        response = Client().get('/#')
        self.assertTemplateUsed(response, 'index.html')

    def test_url_invalid(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    def test_index_contains_accordion(self):
        response = Client().get('/#')
        content = response.content.decode('utf-8')
        self.assertIn('accordion',content)
        self.assertIn('set',content)
        self.assertIn('content',content)
        self.assertIn('Aktifitas', content)
        self.assertIn('Pengalaman', content)
        self.assertIn('Prestasi', content)

