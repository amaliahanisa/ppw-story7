from django.urls import path
from . import views

app_name = 'Story7App'

urlpatterns = [
    path('', views.index, name = 'index'),
]
