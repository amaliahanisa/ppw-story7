from django.urls import path
from .views import book

appname = 'Story8App'

urlpatterns = [
    path('', book, name='bookList'),
]